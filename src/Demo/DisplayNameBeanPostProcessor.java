package Demo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class DisplayNameBeanPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessAfterInitialization(Object Bean, String Beanname) throws BeansException {
		System.out.println("In After Inizialization.Bean name is" + Beanname);
		return Bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object Bean, String Beanname) throws BeansException {
System.out.println("In Before Inizialization.Bean name is" + Beanname);
		return Bean;
	}

}
