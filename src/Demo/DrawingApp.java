package Demo;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
public class DrawingApp {

	public static void main(String[] args) {
		// Senza SPRING Triangle triangle = new Triangle();
		
		// CON SPRING BEAN FACTORY:
		//BeanFactory factory = new XmlBeanFactory(new FileSystemResource ("spring.xml"));
		//factory.getBean("triangle");
		//Triangle triangle = (Triangle) factory.getBean("triangle");
		
		//CON SPRING APPL CONTEXT:
		ApplicationContext context = new ClassPathXmlApplicationContext("file:src/spring.xml");
		//Triangle triangle = (Triangle) context.getBean("triangle");
		//Triangle trianglea = (Triangle) context.getBean("triangle-alias");
		//triangle.draw();
		Shape shape = (Shape) context.getBean("circle");
		shape.draw(); 
		//trianglea.draw();
		
		

	}

}
