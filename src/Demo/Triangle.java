package Demo;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;;
public class Triangle implements ApplicationContextAware, BeanNameAware, Shape {
	
	private String type;
	private int height;
	private Point pointA;
	private Point pointB;
	private Point pointC;
	private ApplicationContext context = null;
	//private List<Point> points;
	
	//public List<Point> getPoints(){
	//	return points;
	//}
	
	//public void setPoints(List<Point> points) {
	//	this.points = points;
	//}
	
	public Point getPointA() {
		return pointA;
	}

	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}

	public Point getPointB() {
		return pointB;
	}

	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}

	public Point getPointC() {
		return pointC;
	}

	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}	

	public void draw() {
        System.out.println("Drawing Triangle");
		System.out.println("point A= (" + getPointA().getX()+","+ getPointA().getY()+ ")");
		System.out.println("point B= (" + getPointB().getX()+","+ getPointB().getY()+ ")");	
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
		
	}

	@Override
	public void setBeanName(String beanName) {
	System.out.println("Bean name is:" + beanName);
		
	}
	
}	
		
	/*	public void drawlist() {
		     
			for (Point point : points) {
				System.out.println("point A= (" + point.getX()+","+ point.getY()+ ")");
			}
	}
}


/*
public int getHeight() {
	return height;
}

public Triangle(String type) {
	this.type = type;
}

public Triangle(String type, int height) {
	this.type = type;
	this.height=height;
}

public Triangle(int height) {
	this.height = height;
}


public String getType() {
	return type;
}

/*  CONNESSO ALL XML METODO 1 SETTER INJECTION
public void setType(String type) {
	this.type = type;
}
*/

