package Demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class Circle implements Shape, ApplicationEventPublisherAware {

	private Point center;
	private ApplicationEventPublisher publisher;
	
	public Point getCenter() {
		return center;
	}
	
	@Autowired
	public void setCenter(Point center) {
		this.center = center;
	}
	
	
	public void draw() {
		 System.out.println("Drawing Circle");
		System.out.println("CIrcle: Point is: " + center.getX()+", " + center.getY()+ ")");
	DrawEvent drawEvent = new DrawEvent(this);
		publisher.publishEvent(drawEvent);	
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
	this.publisher = publisher; 
		
	}
	
}
